<?php
/**
 * Connection to DB class
 */

namespace Core\Database;

use PDO;

/**
 * Class Database
 * @package Core\Database
 */
class Database
{
    /**
     * @var null
     */
    public static $instance = null;
    /**
     * @var PDO
     */
    private $conn;

    /**
     * @var string
     */
    private $DB_HOST = '';
    /**
     * @var string
     */
    private $DB_NAME = '';
    /**
     * @var string
     */
    private $DB_USER = '';
    /**
     * @var string
     */
    private $DB_PASS = '';

    public function __construct () {
        $this->initDBParams();
        if(
            !empty($this->DB_HOST) &&
            !empty($this->DB_NAME) &&
            !empty($this->DB_USER) &&
            !empty($this->DB_PASS)
        ){
            $this->conn =  new PDO(
                'mysql:host=' . $this->DB_HOST . ';dbname=' . $this->DB_NAME,
                $this->DB_USER,
                $this->DB_PASS,
                [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
            );
        }else{
            throw new \Error("You do not set connection to database", 500);
        }

    }

    private function __clone () {}
    private function __wakeup () {}

    /**
     * @return Database|\PDO
     */
    public static function getInstance()
    {
        if (self::$instance != null) {
            self::$instance = new Database();
        }

        return new self;
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->conn;
    }

    /**
     * Set value for configuration DB connection
     *@return null
     */
    protected function initDBParams(){
        $DB_HOST = '';
        $DB_NAME = '';
        $DB_USER = '';
        $DB_PASS = '';

        if(file_exists(ROOT_PATH . '/configs/database.php')){
            extract(require(ROOT_PATH . '/configs/database.php'));


            $this->DB_HOST = $DB_HOST;
            $this->DB_NAME = $DB_NAME;
            $this->DB_USER = $DB_USER;
            $this->DB_PASS = $DB_PASS;
        }
    }
}
