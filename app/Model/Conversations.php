<?php
/**
 * Conversations Model
 */

namespace App\Model;

use PDO;

/**
 * Class Conversations
 * @package App\Model
 * @property int id
 * @property int question_id
 * @property int answer_id
 * @property string status
 */
class Conversations extends Model
{
    /**
     * @var string
     */
    public $table = 'conversations';

    /**
     * Conversations constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTable($this->table);
    }

    /**
     * @param array $filters
     * @return array
     */
    public function getConversations($filters = []){
        try {

            $whereQuery = '(true=true)';
            $whereArray = [];
            $limits = '';

            if(!empty($filters['status']) || !empty($filters['type'])){
                if(!empty($filters['status'])){
                    $whereArray[] = $this->table . ".status='" . $filters['status'] . "'";
                }
                if(!empty($filters['type'])){
                    $whereArray[] = "questions.type='" . $filters['type'] . "'";
                }
                $whereQuery = implode(' AND ', $whereArray);
            }

            if(!empty($filters['pagination'])){
                $pagination = $filters['pagination'];

                $totalConversations = $pagination['totalConversations'];
                $max_pages = ceil($totalConversations / $pagination['per_page']);
                if($pagination['current_page'] > 1 && $pagination['current_page'] <= $max_pages){
                    $startFrom =(($pagination['current_page'] - 1) * $pagination['per_page']);

                    $limits = " LIMIT {$startFrom}, {$pagination['per_page']}";
                }else{
                    $limits = " LIMIT 0, {$pagination['per_page']}";
                }
            }

            //work with db here
            $stmt = $this->connection->prepare("SELECT *, `{$this->table}`.id AS conversation_id FROM `{$this->table}`
                                                          LEFT JOIN `questions` ON `questions`.id = `{$this->table}`.question_id
                                                          WHERE  {$whereQuery}
                                                          ORDER BY `questions`.`addtime` DESC {$limits}");
            $stmt->execute();

            if(!empty($stmt->rowCount())){
                $_SESSION['numberRecords'] = $stmt->rowCount();
            }

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC );

            return $result;

        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getConversation(int $id){
        try {

            //work with db here
            $stmt = $this->connection->prepare("SELECT *, `questions`.`message` AS question, `answers`.`message` AS answer, `questions`.`addtime` AS dateAdded, `{$this->table}`.`id` AS conversation_id
                                                          FROM `{$this->table}`
                                                          LEFT JOIN `questions` ON `questions`.`id` = `{$this->table}`.`question_id`
                                                          LEFT JOIN `answers` ON `answers`.`id` = `{$this->table}`.`answer_id`
                                                          WHERE `{$this->table}`.`id` = " . $id);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC );

            return $result;

        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
    }

}