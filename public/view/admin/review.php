<?php
/**
 * Manage review by admin
 */

require_once $_SERVER['DOCUMENT_ROOT'] . 'public/view/' . 'header.php';
?>
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">Feedback</span>
    </nav>
    <div class="container">
        <div class="row justify-content-md-center">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Type</th>
                    <th scope="col">Title</th>
                    <th scope="col">Status</th>
                    <th scope="col">Message</th>
                    <th scope="col">Date</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row"><?php echo $conversion['conversation_id'];?></th>
                    <td><?php echo $conversion['name'];?></td>
                    <td><?php echo $conversion['email'];?></td>
                    <td><?php echo $conversion['type'];?></td>
                    <td><?php echo $conversion['title'];?></td>
                    <td><?php echo $conversion['status'];?></td>
                    <td><?php echo $conversion['question'];?></td>
                    <td><?php echo $conversion['dateAdded'];?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <form class="col-md-6" id="answerForm" method="POST">
                <div class="form-group">
                    <label for="inputAnswer">Answer</label>
                    <textarea class="form-control" id="inputAnswer" name="inputAnswer" placeholder="Input your answer here..." rows="5"><?php echo $conversion['answer'];?></textarea>
                </div>
                <button type="submit" name="submit" class="btn btn-primary"<?php echo ($conversion['status'] != 'waiting' ) ? ' disabled': ''; ?>><i class="fa fa-paper-plane"></i>Send</button>
                <button id="reject_answer" name="reject" class="btn btn-danger" <?php echo ($conversion['status'] != 'waiting' ) ? ' disabled': ''; ?>>Reject</button>
            </form>
        </div>
    </div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . 'public/view/' . 'footer.php';?>