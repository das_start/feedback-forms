<?php
/**
 * Feedback Controller
 */

namespace App\Controller;


use App\Model\Conversations;
use App\Model\Feedback;
use Core\Mail\Mail;

/**
 * Class FeedbackController
 * @package App\Controller
 */
class FeedbackController extends Controller
{
    /**
     * @var Feedback
     */
    protected $model;
    /**
     * @var Mail
     */
    private $mailClient;
    public function __construct()
    {
        $this->model = new Feedback();
        parent::__construct();

        //mail client
        $this->mailClient = new Mail();
    }

    /**
     * Generate main page
     * return mixed content of main page
     */
    public function indexAction(){

        return $this->view->view('review.php');
    }

    /**
     * Function for work with new feedback
     * @return string
     */
    public function sendAction(){
        //get input data
        $inputData = $_POST;

        //validate data
        $isValid = $this->validation($inputData);

        $conversationId = 0;
        $feedbackId = 0;
        $status = false;
        $errors = [];

        if($isValid['isValid']){
            $feedbackModel = new Feedback();
            $conversationModel = new Conversations();

            //prepare data for database, table questions(feedbacks)
            $insertData = [
                'name' => strip_tags($_POST['inputName']),
                'email' => strip_tags($_POST['inputEmail']),
                'type' => strip_tags($_POST['inputType']),
                'title' => strip_tags($_POST['inputTitle']),
                'message' => strip_tags($_POST['inputFeedback']),
            ];
            $feedbackId = $feedbackModel->insert($insertData);

            //prepare data for database, table conversations
            if(!empty($feedbackId)){
                $insertDataConversation = [
                    'question_id' => $feedbackId,
                    'status' => 'waiting',
                ];
                $conversationId = $conversationModel->insert($insertDataConversation);
            }

            if($conversationId){
                $status = true;

                //send admin notification about new feedback
                $this->mailClient->setTheme('New Feedback');
                $this->mailClient->setTo($this->mailClient->supportEmail, 'Support');

                $params['conversationId'] = $conversationId;
                $this->mailClient->setContent($params, 'adminTemplate');
                $this->mailClient->send();
            }

        }else{
            $status = true;
            $errors = $isValid['errorMessages'];
        }

        //answer
        exit( json_encode(['status' => $status, 'errors' => $errors]));
    }

    /**
     * Validation Feedback data
     * @param $data
     * @return array is valid data, if not return text of error with field key
     */
    protected function validation($data){
        $isValid = true;
        $errorMessages = [];

        if(empty($data['inputName'])){
            $isValid = false;
            $errorMessages['inputName'] = "Can't be empty!";
        }else{
            unset($errorMessages['inputName']);
        }

        if(!filter_var($data['inputEmail'], FILTER_VALIDATE_EMAIL)){
            $isValid = false;
            $errorMessages[$data['inputEmail']] = "Invalid Email!";
        }else{
            unset($errorMessages['inputEmail']);
        }

        if(empty($data['inputTitle'])){
            $isValid = false;
            $errorMessages[$data['inputTitle']] = "Can't be empty!";
        }else{
            unset($errorMessages['inputTitle']);
        }

        if(empty($data['inputType'])){
            $isValid = false;
            $errorMessages[$data['inputType']] = "Can't be empty!";
        }else{
            unset($errorMessages['inputType']);
        }

        if(empty($data['inputFeedback'])){
            $isValid = false;
            $errorMessages[$data['inputFeedback']] = "Can't be empty!";
        }else{
            unset($errorMessages['inputFeedback']);
        }


        return ['isValid' => $isValid, 'errorMessages' => $errorMessages];
    }

}