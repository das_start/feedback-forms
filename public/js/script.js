(function ($, root, undefined) {
    //front functions -------------
    function sendFeedback(){
        $.ajax({
            url: '/feedback/send',
            type: 'POST',
            data: $('#feedbackForm').serialize(),
            dataType: 'json',
            success: function(data) {

                // return false;
                if(data.status) {
                    window.location.replace('/main/thank');
                }else{
                    //TODO:Display errors on front part
                }
            },
            error: function(e){
                console.log(e);
            }
        });
    }
    //---------------------

    //admin functions------
    // function rejectAnswer(){
    //     $.ajax({
    //         url: '/admin/reject',
    //         type: 'POST',
    //         data: {},
    //         dataType: 'json',
    //         success: function(data) {
    //
    //             // return false;
    //             if(data.status) {
    //                 window.location.replace('/admin');
    //             }else{
    //                 //TODO:Display errors on front part
    //             }
    //         },
    //         error: function(e){
    //             console.log(e);
    //         }
    //     });
    // }

    //---------------------

    $(function () {

        'use strict';
        console.log('main script is work!');
        // DOM ready, take it away
        $('#feedbackForm').on('submit', function(e){
            e.preventDefault();
            sendFeedback();
        });

        //admin functionality
        $('#conversationFilterForm').on('change', function(){
            $(this).trigger('submit');
        });

    });

})(jQuery, this);
