<?php
/**
 * View parent class
 */

namespace Core;

/**
 * Class View
 * @package Core
 */
class View
{
    /**
     * @param $template
     * @param array|null $data
     */
    public function view($template, $data = array()) {

        $file = TEMPLATE_PATH . $template;

        if (file_exists($file)) {
            extract($data);

            ob_start();

            require($file);

            $output = ob_get_contents();

            ob_end_clean();


            echo $output;
//            return $output;
        } else {
            trigger_error('Error: Could not load template ' . $file . '!');
            exit();
        }
    }
}