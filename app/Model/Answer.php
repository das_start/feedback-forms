<?php
/**
 * Answer Model
 */

namespace App\Model;

/**
 * Class Answer
 * @package App\Model
 * @property int id
 * @property string message
 * @property datetime addtime
 */

class Answer extends Model
{
    public $table = 'answers';

    public function __construct()
    {
        parent::__construct();
        $this->setTable($this->table);
    }
}