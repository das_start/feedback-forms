<?php
/**
 * Admin general template
 */

require_once $_SERVER['DOCUMENT_ROOT'] . 'public/view/' . 'header.php';
?>
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">Feedbacks</span>
    </nav>
    <div class="container">
        <div class="row filter-area">
            <div class="col-sm-1">
                <span>Filter By: </span>
            </div>
            <div class="col-sm-11">
                <form id="conversationFilterForm">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputStatus">Status</label>
                            <select id="inputStatus" name="inputStatus" class="form-control">
                                <option value="">Choose...</option>
                                <option value="waiting" <?php if(!empty($filters['status']) && $filters['status']=='waiting') echo 'selected'; ?>>Waiting</option>
                                <option value="answered" <?php if(!empty($filters['status']) && $filters['status']=='answered') echo 'selected'; ?>>Answered</option>
                                <option value="rejected" <?php if(!empty($filters['status']) && $filters['status']=='rejected') echo 'selected'; ?>>Rejected</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputType">Type</label>
                            <select id="inputType" name="inputType" class="form-control">
                                <option value="">Choose...</option>
                                <option value="complaint" <?php if(!empty($filters['type']) && $filters['type']=='complaint') echo 'selected'; ?>>Complaint</option>
                                <option value="question" <?php if(!empty($filters['type']) && $filters['type']=='question') echo 'selected'; ?>>Question</option>
                                <option value="remark" <?php if(!empty($filters['type']) && $filters['type']=='remark') echo 'selected'; ?>>Remark</option>
                                <option value="other" <?php if(!empty($filters['type']) && $filters['type']=='other') echo 'selected'; ?>>Other</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Status</th>
                    <th scope="col">Type</th>
                    <th scope="col">Date</th>
                    <th scope="col">Settings</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($feedbacks)):?>
                    <?php foreach ($feedbacks as $key => $feedback):?>
                        <tr>
                            <th scope="row"><?php echo $feedback['conversation_id'];?></th>
                            <td><?php echo $feedback['title'];?></td>
                            <td><?php echo $feedback['status'];?></td>
                            <td><?php echo $feedback['type'];?></td>
                            <td><?php echo $feedback['addtime'];?></td>
                            <td><a href="/admin/answer/<?php echo $feedback['conversation_id'];?>" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Manage Feedback</a></td>
                        </tr>
                    <?php endforeach;?>
                <?php else: ?>
                    <tr><td>No Results</td></tr>
                <?php endif;?>
                </tbody>
            </table>
        </div>
        <div class="row">
                <ul class="pagination pagination-lg">
                    <?php $page = 1;?>
                    <?php while($page <= $pagination['maxPages']):?>
                    <li><a href="/admin?page=<?php echo $page . (!empty($currentRequest) ?$currentRequest: '');?>"><?php echo $page;?></a></li>
                    <?php $page++; endwhile;?>
                </ul>
        </div>
    </div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . 'public/view/' . 'footer.php';?>