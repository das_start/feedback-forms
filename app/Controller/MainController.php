<?php
/**
 * General front controller
 */

namespace App\Controller;

/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display main page
     */
    public function indexAction(){
        return $this->view->view('main.php');
    }

    /**
     * Display thank page
     */
    public function thankAction(){
        return $this->view->view('thanks.php');
    }
}