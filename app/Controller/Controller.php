<?php
/**
 * General Controller
 */

namespace App\Controller;


use Core\View;

/**
 * Class Controller
 * @package App\Controller
 */

class Controller
{
    /**
     * @var View
     */
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }


}