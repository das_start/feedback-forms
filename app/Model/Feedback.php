<?php
/**
 * Feedback Model
 */

namespace App\Model;

/**
 * Class Feedback
 * @package App\Model
 * @property int id
 * @property string name
 * @property string email
 * @property string type
 * @property string title
 * @property string message
 * @property datetime addtime
 */

class Feedback extends Model
{
    public $table = 'questions';

    public function __construct()
    {
        parent::__construct();
        $this->setTable($this->table);
    }
}