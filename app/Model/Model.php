<?php
/**
 * Parent model
 */

namespace App\Model;


use Core\Database\Database;
use PDO;

class Model extends Database
{
    public $connection;
    public $table;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $instance = $this::getInstance();
        $this->connection = $instance->getConnection();//$this::getInstance();

    }

    /**
     * @param mixed $table
     */
    public function setTable($table): void
    {
        $this->table = $table;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    public function getAll($filters=[]){
        try {
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->connection->beginTransaction();

            //work with db here
            $selectQuery = "SELECT * FROM {$this->table}";

            $stmt = $this->connection->prepare($selectQuery);

            $stmt->execute();
//            $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC );

            $this->connection->commit();

            return $result;

        } catch (Exception $e) {
            $this->connection->rollBack();
            echo "Error: " . $e->getMessage();
        }
    }
    public function insert(array $data): int{
        try {
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->connection->beginTransaction();

            //work with db here
            $columns = array_keys($data);
            $columnsForPrepareStmt = array_map(function($column) {
                return ':'.$column;
            },$columns);
            $columnsForPrepareStmtString = implode(',', $columnsForPrepareStmt);
            $columnsString = implode(',', $columns);

            $stmt = $this->connection->prepare("INSERT INTO {$this->table} ({$columnsString}) VALUES ( {$columnsForPrepareStmtString})");

            foreach($columnsForPrepareStmt as $key => $columnKey) {
                $columnNameKey = str_replace(':', '',   $columnKey);
                $stmt->bindParam($columnKey, $data[$columnNameKey]);
            }
            $stmt->execute();

            $returnLastInsertedID = $this->connection->lastInsertId();
            $this->connection->commit();


            return $returnLastInsertedID;

        } catch (Exception $e) {
            $this->connection->rollBack();
            echo "Error: " . $e->getMessage();
        }
    }
    public function update(array $data, $where){
        try {
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->connection->beginTransaction();

            $columns = array_keys($data);
            $columnsForPrepareStmt = array_map(function($column) {
                return $column . '=:'.$column;
            },$columns);
            $columnsForPrepareStmtString = implode(',', $columnsForPrepareStmt);

            $whereColumns = array_keys($where);
            $whereCondition = array_map(function($w) {
                return $w . '=:' . $w;
            },$whereColumns);
            $whereConditionString = implode(',', $whereCondition);

            $sqlQuery = "UPDATE {$this->table} SET {$columnsForPrepareStmtString} WHERE {$whereConditionString};";

            $stmt = $this->connection->prepare($sqlQuery);

            $mergeColumsAndWhereCondiition = array_merge($data, $where);




            $stmt->execute($mergeColumsAndWhereCondiition);

            $this->connection->commit();

            return true;

        } catch (Exception $e) {
            $this->connection->rollBack();
            echo "Error: " . $e->getMessage();
            return false;
        }
    }
    public function count(){

        $query = "SELECT id FROM `{$this->table}`";

        $stmt = $this->connection->prepare( $query );
        $stmt->execute();

        $num = $stmt->rowCount();

        return $num;
    }
}
