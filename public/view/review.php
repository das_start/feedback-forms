<?php
/**
 * Review Template
 */
require_once 'header.php';
?>
<div class="container">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">Leave your feedback</span>
    </nav>
    <div class="row justify-content-md-center">
<!--    <div class="row justify-content-md-left">-->
        <div class="col-sm-12">
            <form id="feedbackForm" onsubmit="return false;">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" id="inputName" name="inputName" placeholder="Name" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Email</label>
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputTitle">Title</label>
                        <input type="text" class="form-control" id="inputTitle" name="inputTitle" placeholder="Title" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputType">Type of feedback</label>
                        <select id="inputType" name="inputType" class="form-control" required>
                            <option value="" selected>Choose...</option>
                            <option value="complaint">Complaint</option>
                            <option value="question">Question</option>
                            <option value="remark">Remark</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputFeedback">Feedback</label>
                    <textarea class="form-control" id="inputFeedback" name="inputFeedback" placeholder="Input your feedback here..." rows="5" required></textarea>
                </div>
                <button type="submit" id="submit"  class="btn btn-primary"><i class="fa fa-paper-plane"></i>Send</button>
            </form>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

