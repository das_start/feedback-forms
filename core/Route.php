<?php
/**
 * Route controller
 */

namespace Core;


use App\Controller;

/**
 * Class Route
 * @package Core
 */
class Route
{
    static function start()
    {
        // default controller and action
        $controller_name = 'Main';
        $action_name = 'index';

        // get controller and action from request uri
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if ( !empty($routes[1]) )
        {
            $controller_name = $routes[1];
            if(strpos($controller_name, "?")){
                $controller_name = explode('?', $controller_name)[0];
            }
        }


        if ( !empty($routes[2]) )
        {
            $action_name = $routes[2];
            if(strpos($action_name, "?")){
                $action_name = explode('?', $action_name)[0];
            }
        }

        // add prefix
        $controller_name = ucfirst($controller_name) . 'Controller';
        $action_name = $action_name . 'Action';

        $controller_file = $controller_name.'.php';
        $controller_path = APP_PATH . "Controller/" . $controller_file;

        if(file_exists($controller_path) && !class_exists($controller_name))
        {
            require_once $controller_path;

        }
        else
        {
            /*if something wrong redirect to 404 or display error
            */
            trigger_error('Error: Could not load template not find class!');
//            Route::ErrorPage404();
        }


        $controllerNameWithNamespace = 'App\Controller\\' . $controller_name;
        $controller = new $controllerNameWithNamespace;
//        $controller = new $controller_name();
        $action = $action_name;


        if(method_exists($controller, $action))
        {
            // call controller action
            $controller->$action();
        }
        else
        {
            /*if something wrong redirect to 404 or display error
            */
            trigger_error('Error: Could not load template action method not exist!');
//            Route::ErrorPage404();
        }

    }

    function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
    function loadClass($className){
        $class = 'Controller\\'.$className;
        return new $class();
    }
}