<?php
/**
 * Admin Controller
 */

namespace App\Controller;


use App\Model\Answer;
use App\Model\Conversations;
use Core\Mail\Mail;

/**
 * Class AdminController
 * @package App\Controller
 */
class AdminController extends Controller
{
    /**
     * @var Conversations
     */
    private $model;

    /**
     * @var Mail
     */
    private $mailClient;
    public function __construct()
    {
        parent::__construct();
        $this->model = new Conversations();

        //mail client
        $this->mailClient = new Mail();
    }

    /**
     * Display all feedback for admin
     */
    public function indexAction(){
        $conversationModel = $this->model;
        $filters = [];
        $data = [];
        $pagination = [];
        $page = $_GET['page'] ?? 1;
        $currentRequest = [];



        $statusFilter = $_GET['inputStatus'] ?? '';
        $typeFilter = $_GET['inputType'] ?? '';
        $hasFilter = false;

        if(!empty($statusFilter)){
            $data['filters']['status'] = $filters['status'] = $statusFilter;
            $hasFilter = true;
        }
        if(!empty($typeFilter)){
            $data['filters']['type'] = $filters['type'] = $typeFilter;
            $hasFilter = true;
        }

        if(!empty($_GET) && $hasFilter) {//save filters in query string
            $currentRequest = '&' . http_build_query($_GET);
        }

        if(!empty($page)){
            $totalRecords = $_SESSION['numberRecords'] ?? $conversationModel->count();

            if(empty($filters)){//if empty filters reset total counter of records in conversions table
                $totalRecords = $conversationModel->count();
            }

            $pagination = [
                'current_page' => $page,
                'per_page' => FEEDBACKS_PER_PAGE,
                'totalConversations' => $totalRecords,
                'maxPages' => ceil($totalRecords / FEEDBACKS_PER_PAGE),
            ];
            $filters['pagination'] = $pagination;
        }

        $getAllFeedbacks = $conversationModel->getConversations($filters);//$feedbackModel->getAll();

        $data['feedbacks'] = $getAllFeedbacks;
        $data['pagination'] = $pagination;
        $data['currentRequest'] = $currentRequest;

        return $this->view->view('admin/index.php', $data);
    }

    /**
     * Answer page
     */
    public function answerAction(){
        $data = [];
        $answerId = 0;

        $parseUri = explode('/', $_SERVER['REQUEST_URI']);
        $conversionId = intval($parseUri[3]);

        $conversionContent = $this->model->getConversation($conversionId);

        if(!empty($_POST)){
            $answerModel = new Answer();
            $conversionModel = new Conversations();
            $status = 'answered';

            if(!empty($_POST['inputAnswer']) && isset($_POST['submit'])){
                $insertData = [
                    'message' => strip_tags($_POST['inputAnswer']),
                ];
                $answerId = $answerModel->insert($insertData);
            }elseif(isset($_POST['reject'])){
                $status = 'rejected';
            }

            //update conversations
            $updateData = [
                'status' => $status
            ];

            if(!empty($answerId)){
                $updateData['answer_id'] = $answerId;

                //send notification to user about answer
                $this->mailClient->setTheme('Answer to Feedback');
                $this->mailClient->setTo($conversionContent['email'], 'alex');

                $params['name'] = $conversionContent['name'];
                $params['answer'] = $insertData['message'];
                $params['status'] = $status;
                $this->mailClient->setContent($params, 'userTemplate');
                $this->mailClient->send();
            }

            $whereCondition = [
                'id' => $conversionId
            ];
            $conversionModel->update($updateData, $whereCondition);

            if($status != 'waiting'){
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /admin');
            }
        }

        $data['conversion'] = $conversionContent;

        return $this->view->view('/admin/review.php', $data);
    }

}