<?php
session_start();
//define variables
define('ROOT_PATH', realpath(__DIR__));
define('TEMPLATE_PATH', ROOT_PATH . '/public/view/');
define('APP_PATH', ROOT_PATH . '/app/');
define('GENERAL_DOMAIN', 'http://vakoms.com.local/');
define('FEEDBACKS_PER_PAGE', 5);

require __DIR__ . '/vendor/autoload.php';

\Core\View::class;

\Core\Route::start();

?>