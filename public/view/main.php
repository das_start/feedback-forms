<?php
/**
 * Default Template
 */
require_once 'header.php';
?>
    <div class="container">
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1">Welcome!</span>
        </nav>
        <div class="row justify-content-md-center">
<!--            <div class="col">Choose action:</div>-->
            <div class="col">
                <div class="row">
                    <div class="col">
                        <a href="/feedback" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Leave Feedback</a>
                    </div>
                    <div class="col">
                        <a href="/admin" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Manage Feedback</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once 'footer.php';?>