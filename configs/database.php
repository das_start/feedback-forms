<?php
/**
 * Database config
 */

return [
    'DB_HOST' => '127.0.0.1',
    'DB_NAME' => 'DB_NAME',
    'DB_USER' => 'DB_USER',
    'DB_PASS' => 'DB_PASS',
];