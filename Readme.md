Task:
For this  task, you will need to create a feedback form. The task has to be done using pure php, without any frameworks. 

User can submit  feedback form with his question to admin. Admin will be able to review all feedbacks and answer them.

User-Side
The Feedback Form shall list the following fields: 
Name
Email (valid email)
Type of feedback (possible values - complaint, question, remark, other)
Title
Text
All fields are required.

If user submits valid form, he will be redirected to “Thanks” page.
Also, after user submitted valid form, admin will receive notification email with link to “answer” page in admin-side.

Admin-Side
Admin side area should be protected by login/password.
On the page with feedbacks, all the feedbacks must be displayed with pagination and sorted by date (newer come first). Also on this page each feedback will have title, indicator if it has been answered already, indicator if it has been rejected by admin and button/link to “answer” page. 
On the page with all feedbacks there have to be filters by feedback status (waiting, answered, rejected) and by feedback type (complaint, question, remark, other). 
On the answer page admin will see all fields user submitted. He will be able either to reject feedback or answer it. 
Admin won’t be able to answer feedbacks that have been already rejected/answered. If admin answer the feedback, then response will be sent to user’s email. 
Sending emails functionality have to be done using mailtrap service (https://mailtrap.io/)



Recommendations.

Will be great (but not necessary) if you use such things as Ajax, SASS, TDD approach. 

Notes.

Work should be deployed to public (or private with giving access) repository into github/gitlab/bitbucket/etc.
The look of pages is not important, but if you wish you can use bootstrap.
