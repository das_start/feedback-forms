<?php
/**
 * Thank you page
 */
require_once 'header.php';
?>
    <div class="container">
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1"></span>
        </nav>
        <div class="row justify-content-md-center">
            <!--            <div class="col">Choose action:</div>-->
            <div class="col">
                <div class="row">
                    <div class="col">
                        <h1>Thank you for your feedback!</h1>
                    </div>
                </div>
                <div class="row">
                    <a href="/feedback" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Leave another Feedback</a>
                </div>
            </div>
        </div>
    </div>

<?php require_once 'footer.php';?>