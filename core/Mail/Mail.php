<?php
/**
 * Created by PhpStorm.
 * User: alexdoroshenko
 * Date: 2019-08-09
 * Time: 16:52
 */

namespace Core\Mail;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail
{
    /**
     * @var PHPMailer
     */
    protected $_mail = null;
    /**
     * @var Environment|null
     */
//    protected $_twig = null;
    /**
     * @var bool
     */
//    protected $_debugs = false;

    protected $_sendEmailAddress = null;
    public $supportEmail = '';

    public function __construct()
    {
        $smtp = [];
        $adminData = [];
        //load configs
        if(file_exists(ROOT_PATH . '/configs/mail.php')) {
            extract(require(ROOT_PATH . '/configs/mail.php'));
        }

        if(!empty($smtp)) {
            $this->_mail = new PHPMailer(true);

            $this->_mail->Host = $smtp['host'];
            $this->_mail->SMTPAuth = true;
            $this->_mail->Username = $smtp['username'];
            $this->_mail->Password = $smtp['password'];
//            $this->_mail->SMTPSecure = 'tls';
            $this->_mail->Port = $smtp['port'];


            if(!empty($adminData)) {
                $this->supportEmail = $adminData['email'];
                $this->_mail->setFrom($adminData['email'], $adminData['name']);
            }
        }

        //debug code
//        echo "<pre>";
//        print_r($this->userTemplate(['name' => 'Alex', 'answer' => 'faster faster']));
//        echo "</pre>";
//        die('stop!');
    }

    public function setTo($email, $name = null) {
        if(!$this->_sendEmailAddress) {
            $this->_sendEmailAddress = $email;
        }
        $this->_mail->addAddress($email, $name);
        return $this;
    }

    public function setFrom($email, $name) {
        $this->_mail->setFrom($email, $name);
        return $this;
    }

    public function setCopy($email, $name = null) {
        $this->_mail->addCC($email, $name);
        return $this;
    }

    public function setSecretCopy($email, $name = null) {
        $this->_mail->addBCC($email, $name);
        return $this;
    }

    public function setTheme($title) {
        $this->_mail->Subject = $title;
        return $this;
    }

    /***
     * @param string $template The template
     * @param array $params
     * @return $this
     */
    public function setContent( $params = [], $template='' ) {
        $this->_mail->isHTML(true);
//        $mailContent = $this->_twig->load($pathToTwig);
        if($this->_mail->Subject) {
            $params['titleMessage'] = $this->_mail->Subject;
        }
        $this->_mail->Body = $this->$template($params);//$mailContent->render($params);
        /* @see http://php.net/manual/en/function.strip-tags.php#refsect1-function.strip-tags-notes */
        $this->_mail->AltBody = strip_tags($this->_mail->Body);

        return $this;
    }

    public function setTextContent($textContent, $params = []) {
        $this->_mail->isHTML(false);
        $this->_mail->Body = $this->_mail->AltBody
            = str_replace(array_keys($params), array_values($params), $textContent);
        return $this;
    }

    /**
     * @param bool
     */
    private function _validateEmail($mail) {

    }

    /**
     * @return bool
     */
    public function send() {
        try {
            return $this->_mail->send();
        } catch (Exception $exception) {
            //debug code
            echo "<pre>";
            print_r("Message could not be sent. Mailer Error: {$this->_mail->ErrorInfo}");
            echo "</pre>";
            return false;
        }
    }

    /**
     * @param null|array $to - lists or sing. Example mail@host.com <br> [ Name User => mail@host.com, Secode User => mail_secode@host.com ]
     * @param null $theme
     * @return Mail
     */
    public static function mail($to = null, $theme = null) {
        $mailer = new self();
        if(is_array($to)) {
            foreach ($to as $name => $email) {
                if(!$email) {
                    continue;
                }
                $mailer->setTo($email, (is_numeric($name) ? null : $name));
            }
        } else {
            $mailer->setTo($to);
        }

        $mailer->setTheme($theme);
        return $mailer;
    }

    public function adminTemplate($params = []){
        $feedbackLink = GENERAL_DOMAIN . 'admin/answer/' . $params['conversationId'];

        $templateContent = "<h4>Hi, Admin</h4>";
        $templateContent .= "<p>You have new feedback!</p>";
        $templateContent .= "<p>For manage feedback, click next <a href='{$feedbackLink}'>link</a>!</p>";
        $templateContent .= "<p>Thank you!";

        return $templateContent;
    }

    public function userTemplate($params = []){

        $templateContent = "<h4>Hi, {$params['name']}</h4>";
        $templateContent .= "<p>We have answer to your feedback:</p>";
        $templateContent .= "<span>{$params['answer']}</span></br></br>";
        $templateContent .= "<p>Feedback status changed to: {$params['status']}</p>";
        $templateContent .= "<p>Thank you!";

        return $templateContent;
    }



}